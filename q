#!/usr/bin/env bash
# q by Leveck 20171210
# gzipchk addition by Tomasino 20171211

gzipchk(){
curl -sILH 'Accept-Encoding: gzip,deflate' "$@" | grep 'Content-Encoding:';
}
if [[ $(gzipchk "$@") ]]; then
curl -A "Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420+ (KHTML, like Gecko) Version/3.0 Mobile/1A543a Safari/419.3" -H "Accept-Encoding: gzip" -s --raw -N "$1" | gunzip - | html2text -width 67 -nobs -style pretty | perl -n -mHTML::Entities -e ' ; print HTML::Entities::decode_entities($_) ;' | cat -s | less
else
curl -A "Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420+ (KHTML, like Gecko) Version/3.0 Mobile/1A543a Safari/419.3" -s --raw -N "$1" | html2text -width 67 -nobs -style pretty | perl -n -mHTML::Entities -e ' ; print HTML::Entities::decode_entities($_) ;' | cat -s | less
fi